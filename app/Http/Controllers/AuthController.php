<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar() {
        return view('page.form');
    }

    public function welcome(Request $request) {
        $name1 = $request['nama1'];
        $name2 = $request['nama2'];
        return view('welcome1', compact('name1', 'name2'));
    }
}
