
<!DOCTYPE html>
<html>
<body>

<div class = 'container'>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form method="POST">
        @csrf
        <label>First name:</label><br></br>
        <input type = 'text' name="nama1"><br></br>
        <label>Last name:</label><br></br>
        <input type = 'text' name="nama2"><br></br>
        <label>Gender:</label><br></br>
        <input type = 'radio' id = 'male' name = 'gender' value = 'male'>Male<br>
        <input type = 'radio' id = 'female' name = 'gender' value = 'female'>Female<br>
        <input type = 'radio' id = 'other' name = 'gender' value = 'other'>Other<br></br>
        <label>Nationality:</label><br></br>
        <select name = 'national' id = 'national'>
            <option>Indonesian</option>
            <option>Singaporean</option>
            <option>Malaysian</option>
            <option>Australian</option>
        </select><br></br>
        <label>Language Spoken:</label><br></br>
        <input type = 'checkbox' id = 'vehicle1' name = 'vehicle1' value = 'indo'>Bahasa Indonesia<br>
        <input type = 'checkbox' id = 'vehicle2' name = 'vehicle2' value = 'eng'>English<br>
        <input type = 'checkbox' id = 'vehicle3' name = 'vehicle3' value = 'otr'>Other<br></br>
        <label>Bio:</label><br></br>
        <textarea cols = '22' rows = '7'></textarea><br>
        <button formaction = '/welcome1'>Sign Up</button>
    </form>
</div>

</body>
</html>