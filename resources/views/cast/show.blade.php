@extends('layout.master')

@section('title')
    Show Cast Id: {{$cast->id}}
@endsection

@section('content')
    <h4>{{$cast->nama}}</h4>
    <p>{{$cast->umur}}</p>
    <p>{{$cast->bio}}</p>
@endsection