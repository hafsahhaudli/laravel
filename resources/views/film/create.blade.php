@extends('layout.master')

@section('title')
    Tambah Film
@endsection

@section('content')
    <form action="/film" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="title">Judul</label>
            <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Title">
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Ringkasan</label>
            <input type="text" class="form-control" name="ringkasan" id="ringkasan" placeholder="Masukkan Body">
            @error('ringkasan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Tahun</label>
            <input type="number" class="form-control" name="tahun" id="tahun" placeholder="Masukkan Body">
            @error('tahun')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Genre</label>
            <input type="text" class="form-control" name="genre" id="genre" placeholder="Masukkan Body">
            @error('genre')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Poster</label>
            <input type="text" class="form-control" name="poster" id="poster" placeholder="Masukkan Body">
            @error('poster')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
@endsection